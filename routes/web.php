<?php

use App\Events\RealTimeMessage;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/key', function() {
  return \Illuminate\Support\Str::random(32);
});

Route::get('/tes', function () {
  $kataPanas = json_decode( App\KataPanas::select('*')->orderBy('jumlah','desc')->limit(5)->get());
  return $kataPanas;
  // tes realtime data
  // event(new App\Events\RealTimeMessage("Tess"));
  // echo "kirim pakket";
  // broadcast(new RealTimeMessage('tes'));
});

Auth::routes();



Route::group(['prefix' => 'api/v1'], function () {
  // ini bagian route api
  Route::resource('post', 'PostController');
  Route::resource('komentar', 'KomentarController');
  Route::resource('katapanas', 'KataPanasController');
  Route::post('likes/{id}/ket/{from}', 'LikeDislikeController@store')->name('likeDislike.store');
});
Route::middleware(['auth'])->group(function () {
  Route::get('profile/{id}', 'ProfileController@show')->name('profile.show');
  Route::get('katapanas/{id}', 'KataPanasController@show')->name('katapanas.show');
  Route::get('home', 'HomeController@index')->name('home');
  Route::get('komentar/{id}', 'KomentarController@create')->name('komentar.create');
  Route::post('komentar', 'KomentarController@store')->name('komentar.store');
});

Route::middleware(['guest'])->group(function () {  
  Route::get('/', function () {
      return view('auth.login');
  });
});