<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatRoom extends Model
{
  protected $table = 'chat_room';
  protected $fillable = ['room_type', 'user_id'];

  public function messages(){
    return $this->hasMany('App\Models\Message', 'chat_room_id')->with('sender');
  }
}
