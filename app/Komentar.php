<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
  protected $table = 'komentar';
  protected $fillable = ['user_id','post_id' , 'isi', 'img_url'];
  
  public function user(){
    return $this->belongsTo('App\User', 'user_id', 'id');
  }

  public function post(){
    return $this->belongsTo('App\Post', 'post_id', 'id');
  }

  public function likes()
  {
    return $this->hasMany('App\LikeDislike', 'komentar_id');
  }
}
