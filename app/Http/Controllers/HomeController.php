<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KataPanas;
use App\Post;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
      // $katapanas = $this->getKatapanas();
      $post = $this->getPost();
      return view('home.index', compact('post'));
    }

    protected function getKatapanas(){
      $kataPanas = KataPanas::orderBy('jumlah', 'DESC')->paginate(5);
      return $kataPanas;
    }

    protected function getPost(){
      $post = Post::orderBy('created_at', 'DESC')->paginate(10);
      return $post;
    }
}
