<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\KataPanas;
use App\KataPanasPost;
use App\Events\RealTimeMessage;
use App\Events\KataPanasReceived;
use Auth;
use Intervention\Image\Facades\Image as Image;

class PostController extends APIBaseController
{
  public function index(){
    $post = Post::all();
    return $this->sendResponse($post, 'get succes');
  }

  public function create(){
   
  }

  public function store(Request $request) {
    $this->validate($request,[
      'isi'=>'required'
    ]);

    $post = Post::create($request->all());
    $post->user_id = Auth::user()->id;
    // $post->user_id = 1;
    $post->isi = $request->get('isi');
    $isi = $request->get('isi');
    
    if (!$isi) {
      return $this->sendError('Isi is required');
    }

    if($file = $request->file('img_url')){
      $makeImage = Image::make($file);
      $path = public_path().'/images/post/';
      $image = time().$file->getClientOriginalName();
      $makeImage->save($path.$image, 72);
      $post->img_url = $image;
    }

    preg_match_all("/#(\\w+)/", $isi, $hashKata);
    foreach ($hashKata[1] as $key => $kata) {
      $this->createOrIncrement($kata, $post->id);
    }
    // tembak events
    $kataPanasBaru = json_decode($this->getKataPanas());

    event(new RealTimeMessage('post baru'));
    event(new KataPanasReceived($kataPanasBaru));

    $post->save();
    return $this->sendResponse($post, 'store succsess');
  }

  public function show($id){
    $post = Post::find($id);
    return $this->sendResponse($post, 'get success');
  }

  public function edit($id)
  {
      
  }

  public function update(Request $request){

  }

  public function destroy($id){
    $post = Post::Find($id);
    $isi = $post->isi;
    $img_url = $post->img_url;

    unlink(public_path().'/images/post/'.$img_url);

    preg_match_all("/#(\\w+)/", $isi, $hashKata);
    foreach ($hashKata[1] as $key => $kata) {
      $this->decrement($kata, $post->id);
    }

    $post->delete();
    return $this->sendResponse($post, 'delete success');
  }

  protected function createOrIncrement(String $kata, $post_id){
    $kata = strtolower($kata);
    $kataPanas = KataPanas::where('kata_panas', $kata)->first();

    if($kataPanas){
      $kataPanas->increment('jumlah');
      KataPanasPost::create([
        'post_id' => $post_id,
        'kata_panas_id' => $kataPanas->id
      ]);
    }else{
      $newkataPanas = KataPanas::create([
        'kata_panas' => $kata,
        'jumlah' => 1
      ]);
      KataPanasPost::create([
        'post_id' => $post_id,
        'kata_panas_id' => $newkataPanas->id
      ]);
    }
  }

  protected function decrement(String $kata, $post_id){
    $kata = strtolower($kata);
    $kataPanas = KataPanas::where('kata_panas',$kata)->first();

    if($kataPanas->jumlah > 1){
      $kataPanas->decrement('jumlah');
    }else{
      KataPanas::where('kata_panas', $kata)->delete();
    }

    KataPanasPost::where('post_id', $post_id)->delete();
  }

  protected function getKataPanas() {
    $kataPanas = KataPanas::select('*')->orderBy('jumlah','desc')->limit(5)->get();
    return $kataPanas;
  }
}
