<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\Post;

class ProfileController extends Controller
{
    public function show($id){
      $profile = Profile::where('user_id', $id)->first();
      $post = Post::where('user_id',$id)->get();
      return view('profile.show', compact('profile', 'post'));
    }
}
