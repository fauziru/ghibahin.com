<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KataPanas;
use App\KataPanasPost;
use App\Post;

class KataPanasController extends APIBaseController
{
  public function index(){
    $kataPanas = KataPanas::select('*')->orderBy('jumlah','desc')->limit(5)->get();
    return $this->sendResponse($kataPanas, 'get success');
  }

  public function create(){
   
  }

  public function store(Request $request){
    
  }

  public function show($id){
    $kataPanas = KataPanas::find($id);
    $post = KataPanasPost::where('kata_panas_id', $id)->orderBy('created_at', 'desc')->get();
    return view('kataterpanas.show', compact('kataPanas', 'post')); 
  }

  public function edit($id)
  {
      
  }

  public function update(Request $request){

  }

  public function destroy($id){
      
  }
}
