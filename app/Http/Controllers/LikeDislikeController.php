<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\LikeDislike;

class LikeDislikeController extends APIBaseController
{
  public function index(){
    
  }

  public function create(){
   
  }

  public function store($id, $from){
    $user = Auth::user()->id;
    
    if ($from == 'post'){
      $postid = $id;
      $komentarid = NULL;
    }else{
      $postid = NULL;
      $komentarid = $id;
    }

    $match = ['user_id' => $user, 'post_id' => $postid, 'komentar_id' => $komentarid];
    $like = LikeDislike::where($match)->first();
    
    if (!$like) {
      $input['user_id'] = $user;
      $input['like_dislike'] = 1;
      $like = LikeDislike::create($input);
      
      if (!$id) {
        return $this->sendError([],'store data error, id is required');
      }

      $from == 'post' ? $like['post_id'] = $id : $like['komentar_id'] = $id;
      $like->save();
      return redirect()->back()->with('status','Posting telah dilike !');
    }else{
      $like->delete();
      return redirect()->back()->with('status',':( !');
    }
  }

  public function show($id){
    
  }

  public function edit($id)
  {
      
  }

  public function update(Request $request){

  }

  public function destroy($id){
      
  }
}
