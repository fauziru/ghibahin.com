<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Komentar;
use App\Post;
use Auth;
use Intervention\Image\Facades\Image as Image;

class KomentarController extends APIBaseController
{
  public function index(){
    $komentar = Komentar::all();
    return $this->sendResponse($komentar, 'get succes');
  }

  public function create($id){
    $post = Post::find($id);
    return view('komentar.create', compact('post'));
  }

  public function store(Request $request){
    $validator = $this->validate($request,[
      'isi'=>'required',
      'post_id' => 'required'
    ]);
    
    $komentar = Komentar::create($request->all());
    $komentar->user_id = Auth::user()->id;
    $komentar->isi = $request->get('isi');
    $isi = $request->get('isi');
    $komentar->post_id = $request->get('post_id');
    
    if (!$isi && $request->post_id) {
      return $this->sendError('Isi and post_id is required');
    }

    if($file = $request->file('img_url')){
      $makeImage = Image::make($file);
      $path = public_path().'/images/komentar/';
      $image = time().$file->getClientOriginalName();
      $makeImage->save($path.$image, 72);
      $komentar->img_url = $image;
    }

    $komentar->save();
    return redirect()->route('home');
  }

  public function show($id){
    $komentar = Komentar::where('post_id', $id)->get();
    return $this->sendResponse($komentar, 'get succes');
  }

  public function edit($id)
  {
      
  }

  public function update(Request $request){

  }

  public function destroy($id){
    $komentar = Komentar::Find($id);
    $isi = $komentar->isi;
    $img_url = $komentar->img_url;

    unlink(public_path().'/images/post/'.$img_url);

    preg_match_all("/#(\\w+)/", $isi, $hashKata);
    foreach ($hashKata[1] as $key => $kata) {
      $this->decrement($kata, $komentar->id);
    }

    $komentar->delete();
    return $this->sendResponse($komentar, 'delete success');
  }
}
