<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\PrivateMessageEvent;
use App\Models\User;
use App\Models\ChatRoom;
use App\Models\RoomMember;
use App\Models\Message;
use App\Models\Receiver;
Use Auth;

class PrivateChatController extends Controller
{
    public function get(Chatroom $chatroom){
      return $chatroom->messages;
    }

    public function index($receiverId){
        $receiver = User::find($receiverId);
        $senderUserId = Auth::user()->id;
        $roomMembers = [$receiverId, $senderUserId];
        sort($roomMembers);
        $roomMembers = implode($roomMembers, ',');
        
        $chatRoom = ChatRoom::where('user_id', $roomMembers)->first();
        if(is_null($chatRoom)) {
            $chatRoom = new ChatRoom;
            $chatRoom->room_type = 'private';
            $chatRoom->user_ids = $roomMembers;
            $chatRoom->save();
            foreach([$receiverId, $senderUserId] as $value) {
                RoomMember::create([
                  'user_id' => $value,
                  'chat_room_id' => $chatRoom->id
                ]);
            }
        }

        return view('private-chat.form', compact('chatRoom', 'receiver'));
    }

    public function store(ChatRoom $chatroom)
    {
        $senderId = Auth::user()->id;
        $roomMembers = collect(explode(',', $chatroom->user_ids));
        $roomMembers->forget($roomMembers->search($senderId));
        $receiverId = $roomMembers->first();

        $message = new Message;
        $message->chat_room_id = $chatroom->id;
        $message->sender_id = $senderId;
        $message->message = request('message');
        $message->save();

        $receiver = new Receiver;
        $receiver->message_id = $message->id;
        $receiver->receiver_id = $receiverId;

        if($receiver->save()) {
            $message = Message::with('sender')->find($message->id);
            broadcast(new PrivateMessageEvent($message))->toOthers();
            return $message;
        } else {
            return 'Something went wrong!!';
        }
    }


}
