<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Receiver;

class Message extends Model
{
  protected $table = 'message';
  public function sender() {
    return $this->hasOne(User::class, 'id', 'sender_id');
  }

  public function receivers() {
    return $this->hasMany(Receiver::class);
  }
}
