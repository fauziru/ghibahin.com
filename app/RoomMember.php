<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomMember extends Model
{
  protected $table = 'room_member';
  protected $fillable = ['chat_room_id', 'user_id'];
  
  /**
  * Get the sender of the message
  */
  public function chatRoom(){
    return $this->belongsTo('App\Models\ChatRoom', 'chat_room_id', 'id');
  }

  public function user(){
    return $this->belongsTo('App\Models\User', 'user_id', 'id');
  }
}
