<?php

function HashWord($isi, $keykata){
  preg_match_all("/#$keykata/i", $isi, $hashKata);
  foreach ($hashKata[0] as $key => $hash) {
    $result = str_replace($hash, '<b class="mark">' . $hash . '</b>', $isi);
  }
  return $result;
}

function StatWarna($stat) {
  return count($stat) > 0 ? 'color: pink' : '';
}

function ReadableDate($created_at) {
  $today = date('Y-m-d H:i:s');
  $dift_s = strtotime($today) - strtotime($created_at);
  $dift_d = floor($dift_s / 86400);

  if($dift_d == 0){
    if ($dift_s < 10) {
      return ' Baru Saja';
    }elseif($dift_s < 60){
      return floor($dift_s).' d';
    }elseif ($dift_s < 3600) {
      return floor($dift_s / 60).' m';
    }elseif ($dift_s < 86400) {
      return floor($dift_s / 3600).' j';
    }
  }else {
    if ($dift_d == 1) {
      return ' Kemarin';
    }elseif ($dift_d <= 7) {
      return $dift_d .' H';
    }elseif ($dift_d <= 31) {
      return floor($dift_d / 7).' M';
    }elseif ($dift_d <= 365) {
      return floor($dift_d / 31).' B';
    }else {
      return date('jS F Y', strtotime($created_at));
    }
  }
}