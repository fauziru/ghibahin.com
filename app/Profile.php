<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
  protected $table = 'profile';
  protected $fillable = ['user_id', 'nama_lengkap', 'email','gender'];
  
  public function user()
  {
    return $this->belongsTo('App\User', 'user_id', 'id');
  }
}
