<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KataPanas extends Model
{
  protected $table = 'kata_panas';
  protected $fillable = ['kata_panas', 'jumlah'];
  public function kataPanasPost()
  {
    return $this->hasMany('App\KataPanasPost', 'kata_panas_id');
  }
}
