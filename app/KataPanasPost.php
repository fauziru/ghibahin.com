<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KataPanasPost extends Model
{
  protected $table = 'kata_panas_post';
  protected $fillable = ['kata_panas_id', 'post_id', 'komentar_id'];
  public function post()
  {
    return $this->belongsTo('App\Post', 'post_id', 'id');
  }
  public function kataPanas()
  {
    return $this->belongsTo('App\KataPanas', 'kata_panas_id', 'id');
  }
}
