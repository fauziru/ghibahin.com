<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
  protected $table = 'post';
  protected $fillable = ['user_id', 'isi', 'img_url'];
  
  public function user()
  {
    return $this->belongsTo('App\User', 'user_id', 'id');
  }

  public function kataPanasPost()
  {
    return $this->hasMany('App\KataPanasPost', 'post_id');
  }

  public function komentar()
  {
    return $this->hasMany('App\Komentar', 'post_id');
  }

  public function likes()
  {
    return $this->hasMany('App\LikeDislike', 'post_id');
  }
}
