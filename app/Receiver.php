<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receiver extends Model
{
  protected $table = 'receiver';
  protected $fillable = ['message_id', 'receiver_id'];
}
