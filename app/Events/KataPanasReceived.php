<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class KataPanasReceived implements ShouldBroadcast
{
    use SerializesModels;

    public $payload;

    public function __construct($data)
    {
      $this->payload = $data;
    }

    
    public function broadcastOn(): Channel
    {
        return new Channel('katapanas-payload');
    }
}
