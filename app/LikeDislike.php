<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikeDislike extends Model
{
  protected $table = 'likes';
  protected $fillable = ['user_id', 'post_id', 'komentar_id', 'like_dislike'];
  
  public function user(){
    return $this->belongsTo('App\User', 'user_id', 'id');
  }
  
  public function post(){
    return $this->belongsTo('App\Post', 'post_id', 'id');
  }

  public function komentar(){
    return $this->belongsTo('App\Komentar', 'komentar_id', 'id');
  }
}
