@extends('layouts.app', ['title'=>'Kata Panas','activePage'=>'trending'])

@section('content')
<div class="row justify-content-center">
  <div class="col-xl-12">
      <div class="card">
        <div class="card-header">
          <h4>Kata Terpanas <span class="font-weight-bold">#{{ $kataPanas->kata_panas }}</span></h4>
        </div>
        <div class="card-body">
          {{-- perulangan disini --}}
          @forelse ($post as $item)
          @php
            if (!$item->post->user) {
              $item->post = $item->komentar;
            }
          @endphp
          <div class="media border-bottom py-3">
            <img src="https://ui-avatars.com/api/?background=random&name={{ $item->post->user->name }}" class="align-self-start rounded-left mr-3" alt="...">
            <div class="media-body">
              <div class="w-100 d-inline-flex justify-content-start">
                <div class="overflow-hidden text-truncate" >
                  <a href="{{ route('profile.show', $item->post->user->id ) }}" class="text-decoration-none">{{ $item->post->user->email }}</a> 
                </div>
                <span class="pl-2" style="color: gray" >{{ ReadableDate($item->post->created_at) }}</span>
                <div class="dropdown ml-auto">
                  <a href="{{ route('komentar.create', $item->post->id ) }}" class="decoration-none mr-1">
                    <i class="fa fa-comment" aria-hidden="true"></i>
                  </a>
                    {{ count($item->post->komentar) }}
                  <a href="#" onclick="event.preventDefault();document.getElementById('likes-form-{{ $item->post->id }}').submit();" class="decoration-none mr-1 ml-1" style="{{ StatWarna($item->post->likes->where('user_id', Auth::user()->id)) }}">
                    <form id="likes-form-{{ $item->post->id }}" action="{{ url('api/v1/likes/'.$item->post->id.'/ket/post') }}" method="POST" style="display: none;">
                      @csrf
                    </form>
                    <i class="fa fa-heart " aria-hidden="true"></i>
                  </a>
                    {{ count($item->post->likes) }}
                  <button class="ml-1 btn btn-secondary btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">Chat</a>
                    <a class="dropdown-item" href="#">Report</a>
                  </div>
                </div>
                {{-- <button type="button" class="btn btn-light" style="" data-toggle="modal" data-target="#modal-pertanyaan">
                  <i class="fa fa-caret-down" aria-hidden="true"></i>
                </button> --}}
              </div>
              <p>{!! HashWord($item->post->isi, $kataPanas->kata_panas) !!}</p>
              @if ($item->post->img_url)
                  <img src="{{ asset('images/post/'.$item->post->img_url) }}" class="img-fluid img-thumbnail"/>
              @endif
            </div>
          </div>
          @empty
            <h3>Data Tidak Tersedia</h3>
            
          @endforelse
      </div>
      </div>
  </div>
</div>
@endsection