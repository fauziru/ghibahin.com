@extends('layouts.master', ['titlebody'=>'Home','activePage'=>'home','activeTree'=>'home'])
@section('title-head','Home')

@section('contents')
    <img src="{{ asset('images/ERD.png')}}" alt="image">
@endsection

@push('scripts')
<script>
    Swal.fire({
        title: "Berhasil!",
        text: "Memasangkan script sweet alert",
        icon: "success",
        confirmButtonText: "Cool",
    });
  Echo.channel('events').listen('RealTimeMessage', (e) =>{
    console.log('realtim message', e.message)
    Swal.fire({
        title: 'Pesan baru',
        text: e.message,
        icon: 'info',
        position: 'top-end',
        showConfirmButton: false,
        toast: true,
        timer: 3000
    })
  });
</script>
@endpush