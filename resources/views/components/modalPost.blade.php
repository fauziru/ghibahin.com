<div class="modal fade" id="modal-post" style="display: none;" aria-modal="true">
  <!-- modal-dialog -->
  <div class="modal-dialog modal-lg">
    <!-- modal-content -->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Post</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="quickFormPost" method="POST" action="{{ url('/api/v1/post') }}">
              @csrf
              
                <div class="form-group">
                  <label for="isi">Post:</label>
                  <textarea class="form-control" name="isi" id="isi" rows="3" placeholder="Silahkan Masukan Post ... gunakan # untuk gunakan fitur kata terpanas"></textarea>
                  <small class="form-text text-muted">Gunakan dengan kata kunci berbeda</small>
                </div>
                
                <div class="form-group">
                  <label for="exampleFormControlFile1">Pilih Gambar:</label>
                  <input type="file" class="form-control-file" name="img_url">
                  <small id="postImg" class="form-text text-muted">Maksimal ukuran gambar: 5mb</small>
                </div>
              
          </form>
      </div>
      <div class="modal-footer justify-content-end">
        <button type="button" class="btn btn-outline-dark btn-default" data-dismiss="modal">Close</button>
        @include('components.partialLoader')
        <button id="submitPost" type="submit" form="quickFormPost" class="btn btn-primary">Submit</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

@push('scripts')
<script>
  $(function () {
      var Toast = Swal.mixin({
              position: 'center',
              showConfirmButton: false,
              timer: 2000
          });
      $('.loader').hide();
      $('#quickFormPost').submit(function(e) {
          e.preventDefault();
          if ($('#quickFormPost').valid()) {    
              $('.loader').show();
              $('#submitPost').hide();
              let data = $('#quickFormPost').serialize();
              console.log('data', $('input[name=img_url]').val());
              var formData = new FormData(this);
              formData.append('img_url', $('input[name=img_url]').val());
              console.log('form-data', formData);
              $.ajax({
                   type:'POST',
                   url: '/api/v1/post',
                   data: formData,
                   dataType: 'json',
                   contentType: false,
                   processData: false,
                   success : function(e){
                       Toast.fire({icon:'success',title: 'Post berhasil terkirim'});
                       $('#submitPost').show();
                       $('.loader').hide();
                       $('#modal-post').modal('hide');
                       console.log(e.message);
                   },
                   error: function(){
                       Toast.fire({icon:'error',title: e.messages});
                       $('#submitPost').show();
                       $('.loader').hide();
                       console.log('data post', data);
                       console.log('gagal', e);
                   }
               });
          }
      });

      $('#quickFormPost').validate({
          rules:{
              isi:{
                  required: true
              },
              postImg:{
                 accept: 'image/*'
              }
          },
          messages:{
              isi:{
                  required:"Pertanyaan tidak boleh kosong"
              }
          },
          errorElement: 'span',
          errorPlacement: function (error, element) {
              error.addClass('invalid-feedback');
              element.closest('.form-group').append(error);
          },
          highlight: function (element, errorClass, validClass) {
              $(element).addClass('is-invalid');
          },
          unhighlight: function (element, errorClass, validClass) {
              $(element).removeClass('is-invalid');
          }
      });
  });
</script>
@endpush
