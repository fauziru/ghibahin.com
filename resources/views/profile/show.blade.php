@extends('layouts.app', ['title'=>'Profile','activePage'=>'profile'])

@section('content')
<div class="row justify-content-center">
  <div class="col-xl-12">
      <div class="card">
        <div class="card-header">
          <h4>Profile {{ $profile->nama_lengkap }}</h4>
        </div>
        <div class="card-body">
          @forelse ($post as $item)
          <div class="media border-bottom py-3">
            <img src="https://ui-avatars.com/api/?background=random&name={{ $item->user->name }}" class="align-self-start rounded-left mr-3" alt="...">
            <div class="media-body">
              <div class="w-100 d-inline-flex justify-content-start">
                <div class="overflow-hidden text-truncate" >
                  <a href="{{ route('profile.show', $item->user->id) }}" class="text-decoration-none">{{ $item->user->email }}</a> 
                </div>
                <span class="pl-2" style="color: gray" >{{ ReadableDate($item->created_at) }}</span>
                <div class="dropdown ml-auto">
                  <a href="{{ route('komentar.create', $item->id) }}" class="decoration-none mr-1">
                    <i class="fa fa-comment" aria-hidden="true"></i>
                  </a>
                  {{ count($item->komentar) }}
                  <a href="#" onclick="event.preventDefault();document.getElementById('likes-form-{{ $item->id }}').submit();" class="decoration-none mr-1 ml-1" style="{{ StatWarna($item->likes->where('user_id', Auth::user()->id)) }}">
                    <form id="likes-form-{{ $item->id }}" action="{{ url('api/v1/likes/'.$item->id.'/ket/post') }}" method="POST" style="display: none;">
                      @csrf
                    </form>
                    <i class="fa fa-heart " aria-hidden="true"></i>
                  </a>
                    {{ count($item->likes) }}
                  <button class="ml-1 btn btn-secondary btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">Chat</a>
                    <a class="dropdown-item" data-toggle="collapse" href="#collapseThree{{ $item->id }}" aria-expanded="false" aria-controls="collapseThree{{ $item->id }}">Komentar</a>
                    <a class="dropdown-item" href="#">Report</a>
                  </div>
                </div>
                {{-- <button type="button" class="btn btn-light" style="" data-toggle="modal" data-target="#modal-pertanyaan">
                  <i class="fa fa-caret-down" aria-hidden="true"></i>
                </button> --}}
              </div>
              <p>{{ $item->isi }}</p>
              @if ($item->img_url)
                  <img src="{{ asset('images/post/'.$item->img_url) }}" class="img-fluid img-thumbnail"/>
              @endif
              <div id="collapseThree{{ $item->id }}" class="collapse">
                @php
                    $komentar = App\Komentar::where('post_id', $item->id)->get();
                @endphp
                @foreach($komentar as $komen)
                  <div class="media border-bottom py-3">
                    <img src="https://ui-avatars.com/api/?background=random&name={{ $komen->user->name }}" class="align-self-start rounded-left mr-3" alt="...">
                    <div class="media-body">
                      <div class="w-100 d-inline-flex justify-content-start">
                        <div class="overflow-hidden text-truncate" >
                          <a href="{{ route('profile.show', $komen->user->id) }}" class="text-decoration-none">{{ $komen->user->email }}</a> 
                        </div>
                        <span class="pl-2" style="color: gray" >{{ ReadableDate($komen->created_at) }}</span>
                        <div class="dropdown ml-auto">
                          <a href="#" onclick="event.preventDefault();document.getElementById('likes-form-{{ $komen->id }}').submit();" class="decoration-none mr-1 ml-1" style="{{ StatWarna($komen->likes->where('user_id', Auth::user()->id)) }}">
                            <form id="likes-form-{{ $komen->id }}" action="{{ url('api/v1/likes/'.$komen->id.'/ket/post') }}" method="POST" style="display: none;">
                              @csrf
                            </form>
                            <i class="fa fa-heart " aria-hidden="true"></i>
                          </a>
                            {{ count($komen->likes) }}
                          <button class="ml-1 btn btn-secondary btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Chat</a>
                            <a class="dropdown-item" href="#">Report</a>
                          </div>
                        </div>
                        {{-- <button type="button" class="btn btn-light" style="" data-toggle="modal" data-target="#modal-pertanyaan">
                          <i class="fa fa-caret-down" aria-hidden="true"></i>
                        </button> --}}
                      </div>
                      <p>{{ $komen->isi }}</p>
                      @if ($komen->img_url)
                          <img src="{{ asset('images/komentar/'.$komen->img_url) }}" class="img-fluid img-thumbnail"/>
                      @endif
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
          </div>
          @empty
            <h3>Data Tidak Tersedia</h3>
          @endforelse
        </div>
      </div>
  </div>
</div>
@endsection