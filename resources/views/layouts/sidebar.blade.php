<div class="">
  <div class="row justify-content-center">
    <div class="col-xl-12">
      <div class="card" style="border-radius: 20px">
        <div class="">
          <div class="text-center py-3 d-lg-inline-flex px-4 w-100 justify-content-center overflow-hidden">
            @auth
            @php
              $profile = App\Profile::where('user_id', Auth::user()->id)->first();
            @endphp
            @if ($profile->user->img_url)
              <img src="{{ asset('images/user_img/'.$profile->user->img_url) }}" class="img-size-50 rounded-circle img-fluid" alt="profile">
            @else
              <img src="https://ui-avatars.com/api/?background=0D8ABC&color=fff&name={{ $profile->user->name }}" class="img-size-50 rounded-circle img-fluid" alt="profile">
            @endif
            <h5 class="align-self-center"><a class="ml-lg-3 text-decoration-none" href="#">{{ $profile->user->name }}</a></h5>
          </div>
          <a class="dropdown-item {{ $activePage == 'home' ? 'active' : '' }}" href="{{ route('home') }}">HOME</a>
          <a class="dropdown-item {{ $activePage == 'profile' ? 'active' : '' }}" href="{{ route('profile.show', $profile->user->id) }}">PROFILE</a>
          <a class="dropdown-item {{ $activePage == 'roomchat' ? 'active' : '' }}" href="#">CHAT</a>
          <a class="dropdown-item {{ $activePage == 'trending' ? 'active' : '' }}" href="#">TRENDING</a>
          @endauth
        </div>
      </div>
    </div>
  </div>
</div>