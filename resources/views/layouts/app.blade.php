<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} - {{ $title }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <script src="https://kit.fontawesome.com/6a8a2c0076.js" crossorigin="anonymous"></script>

    <!-- Styles -->
    <!-- preloader -->
    <link rel="stylesheet" href="{{ asset('css/preloader.css')}}">
    <!-- partialLoader -->
    <link rel="stylesheet" href="{{ asset('css/partialLoader.css')}}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @stack('styles')
</head>
<body>
  @include('layouts.preloader')
  <div id="app">
    {{-- navbar --}}
    @include('layouts.navbar')
    @auth
    <div class="container-xl" style="margin-top: 3.5rem">
      <div class="row flex-xl">

        {{-- sidebar --}}
        <div class="col-md-3 col-xl-3 bd-sectbar">
          @include('layouts.sidebar')
        </div>
        
        {{-- feature --}}
        <div class="col-md-3 col-xl-3 bd-fetbar">
          @include('layouts.featurebar')
        </div>

        {{-- content --}}
        <div class="col-md-6 col-xl-6 bd-content">
          <main class="py-4" style="">
            @yield('content')
          </main>
        </div>
      @endauth
      @guest
        <div style="margin-top: 5rem">
          @yield('content')
        </div>
      @endguest

      </div>
    </div>
  </div>

  <script src="{{ asset('js/app.js') }}"></script>
  <script src="{{ asset('js/swal.min.js')}}"></script>
  <script src="{{ asset('js/jquery-validation/jquery.validate.min.js')}}"></script>
  <script src="{{ asset('js/jquery-validation/additional-methods.min.js')}}"></script>
  <script>
    (function($) {
      var $window = $( window )
      $window.on('load', function()  { 
        $('.status').fadeOut();
        $('.preloader').fadeOut('slow'); 
      }); 
    })(jQuery)
  </script>
  @stack('scripts')
</body>
</html>
