<div class="">
  <div class="row justify-content-center">
      <div class="col-xl-12">
          <div class="card" style="border-radius: 20px">
              <div class="card-body">
                  Kata Terpanas
              </div>
                <ul class="list-group list-group-flush">
                  @auth  
                    <item-katapanas :data="katapanas"></item-katapanas>
                  @endauth         
                </ul>
              {{-- @include('layouts.partialLoader') --}}
          </div>
      </div>
  </div>
  <span style="color: gray">Copyright © 2021 fauziru</span>
</div>

@push('scripts')
  <script>
    $(function(){
      // $.ajax({
      //   type : 'GET',
      //   url: `/katapanas`,
      //   success : function(data){
      //     pageNumber +=1;
      //     if(data.html.length == 0){
      //       Toast.fire({icon:'info',title:'Semua Data Sudah Dimuat'});
      //       $('.loader').hide();
      //     }else{
      //       $('.timeline').append(data.html);
      //       $('.loader').hide();
      //               $('#memuat').show();
      //             }
      //   }
      // });
    });
  </script>
@endpush