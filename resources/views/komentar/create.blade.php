@extends('layouts.app', ['title'=>'Home','activePage'=>'home'])

@section('content')
<div class="">
  <div class="row justify-content-center">
      <div class="col-xl-12">
          <div class="card">
              <div class="card-header d-inline-flex justify-content-between">
                <h4>Buat Komentar</h4>
              </div>
              <div class="card-body">
                {{-- media --}}
                <div class="media border-bottom py-3">
                  <img src="https://ui-avatars.com/api/?background=random&name={{ $post->user->name }}" class="align-self-start rounded-left mr-3" alt="...">
                  <div class="media-body">
                    <div class="w-100 d-inline-flex justify-content-start">
                      <div class="overflow-hidden text-truncate" >
                        <a href="{{ route('profile.show', $post->user->id) }}" class="text-decoration-none">{{ $post->user->email }}</a> 
                      </div>
                      <span class="pl-2" style="color: gray" >{{ ReadableDate($post->created_at) }}</span>
                      <div class="dropdown ml-auto">
                        <a href="#" class="decoration-none mr-1">
                          <i class="fa fa-comment" aria-hidden="true"></i>
                        </a>
                        {{ count($post->komentar) }}
                        <a href="#" onclick="event.preventDefault();document.getElementById('likes-form-{{ $post->id }}').submit();" class="decoration-none mr-1 ml-1" style="{{ StatWarna($post->likes->where('user_id', Auth::user()->id)) }}">
                          <form id="likes-form-{{ $post->id }}" action="{{ url('api/v1/likes/'.$post->id.'/ket/post') }}" method="POST" style="display: none;">
                            @csrf
                          </form>
                          <i class="fa fa-heart " aria-hidden="true"></i>
                        </a>
                          {{ count($post->likes) }}
                        <button class="ml-1 btn btn-secondary btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="#">Chat</a>
                          <a class="dropdown-item" href="#">Report</a>
                        </div>
                      </div>
                      {{-- <button type="button" class="btn btn-light" style="" data-toggle="modal" data-target="#modal-pertanyaan">
                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                      </button> --}}
                    </div>
                    <p>{{ $post->isi }}</p>
                    @if ($post->img_url)
                        <img src="{{ asset('images/post/'.$post->img_url) }}" class="img-fluid img-thumbnail"/>
                    @endif
                  </div>
                </div>

                {{-- form komentar --}}
                <form id="quickFormKomentar" method="POST" action="{{ route('komentar.store') }}">
                  @csrf
                  <input type="hidden" name="post_id"  value="{{ $post->id }}" />
                    <div class="form-group">
                      <label for="isi">Komentar:</label>
                      <textarea class="form-control" name="isi" id="isi" rows="3" placeholder="Silahkan Masukan Komentar ... gunakan # untuk gunakan fitur kata terpanas"></textarea>
                      <small class="form-text text-muted">Gunakan dengan kata kunci berbeda</small>
                    </div>
                    
                    <div class="form-group">
                      <label for="exampleFormControlFile1">Pilih Gambar:</label>
                      <input type="file" class="form-control-file" name="img_url">
                      <small class="form-text text-muted">Maksimal ukuran gambar: 5mb</small>
                    </div>
                </form>
                <button id="submitPost" type="submit" form="quickFormKomentar" class="btn btn-primary ml-auto">Submit</button>
              </div>
          </div>
      </div>
  </div>
@endsection